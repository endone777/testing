<?php 
require_once 'vendor/autoload.php';

// ini_set("display_errors" , 0); 

use Functions\XlsExchange as FunctionsXlsExchange;

class Abc {

    public function test(){

        $xls = new FunctionsXlsExchange;
        
            $xls
                ->setInputFile('files/order.json')
                ->setOutputFile('files/items.xlsx')
                ->ean13_verify()
                ->ftp_test()
                ->export()
            ;

    }

}

$new = new Abc();
$new->test();
