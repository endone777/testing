<?php

namespace Functions;

use Ced\Validator\Barcode;

class Codes {

    public $connect = false;

    public function __construct()
    {
        $this->validator = new Barcode();
    }

    public function ftp_connection($ftp_host = 'localhost' , $ftp_login = 'user' , $ftp_password = 'user' , $ftp_dir = '' , $ftp_uploaded_file = 'files/items.xlsx'){

        $connection = ftp_connect($ftp_host);

        if(!$connection){
            echo 'Проблема с FTP подключением, файл сохранен локально';
        }else{
            if (ftp_login($connection, $ftp_login, $ftp_password)) {
                if (ftp_put($connection, $ftp_dir . '/items.xlsx', $ftp_uploaded_file, FTP_BINARY)) {
                    echo "$ftp_uploaded_file успешно загружен на сервер\n $ftp_dir/items.xlsx";
                } else {
                    echo "Не удалось загрузить $ftp_uploaded_file на сервер\n";
                }
            } 
        }

    }

    public function ean13_cheker ($code){

        $this->validator->setBarcode($code);

        if ($this->validator->isValid()) {
            return true;
        } else {
            return false;
        }

    }

}