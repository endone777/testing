<?php 

namespace Functions;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class XlsExchange extends Codes {

    static $ftp_host = 'localhost';
    static $ftp_login = 'user';
    static $ftp_password = 'user';
    static $ftp_dir = '/dir';

    public function setInputFile($dir){
        $json_format = json_decode(file_get_contents($dir) );
            $this->files = $json_format;
        return $this;
    }

    public function setOutputFile($dir_file){
            $this->dir_file = $dir_file;
        return $this;
    }
    
    public function ean13_verify(){
        
            foreach ($this->files->items as $item){
                $this->good[] = $item->item->barcode;
                if(Codes::ean13_cheker($item->item->barcode) ){
                    $this->ean13_trusted[] = $item;
                }
            }

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue('A1', 'id');
            $sheet->setCellValue('B1', 'ШК');
            $sheet->setCellValue('C1', 'Название');
            $sheet->setCellValue('D1', 'Кол-во');
            $sheet->setCellValue('E1', 'Сумма');
            $cell = 2;
            foreach($this->ean13_trusted as $item){
                $sheet->setCellValue('A'.$cell, $item->item->id);
                $sheet->setCellValue('B'.$cell, $item->item->external_article);
                $sheet->setCellValue('C'.$cell, $item->item->name);
                $sheet->setCellValue('D'.$cell, $item->quantity);
                $sheet->setCellValue('E'.$cell, $item->amount);
                $cell++;
            }

            $writer = new Xlsx($spreadsheet);
            $writer->save($this->dir_file);

        return $this;

    }

    public function ftp_test(){

        $this->ftp_test = Codes::ftp_connection(self::$ftp_host, self::$ftp_login , self::$ftp_password , self::$ftp_dir );

        return $this;

    }

    public function export(){

        if(!$this->ftp_test){

            return "Ошибка подключения к FTP, файл сохранен локально {$this->dir_file}";

        }

    }

}